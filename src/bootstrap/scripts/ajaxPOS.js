/*
* ajax calls that insert information into the tab content 'PieChartPOS'
* visualisation is a pie-chart
*/

// when the 'POS' tab is clicked on, the function starts to add the data from the API /pos
function ajaxPos(){
$.ajax({
        type: "GET",
        url: "http://api.prg2021.texttechnologylab.org/pos",
        // does something on succeeding API access
        success: function (data) {
            // adds the data 1 by 1 into the chart till its done and ready to visualize
            var pos = data.result;
            var label;
            var count;
            for (let i = 0; i < pos.length; i++) {
                label = pos[i].pos;
                count = pos[i].count;
                addData(PieChartPOS, label, count);
            }
        },
        // prints the error on the console
        error: function (error) {
            console.log("Error ${error}");
        }
        });
}

/*
* the parameter of the function is the input from the textfield 'party'
* filters the API Request with the input
*/
function filterPOSParty(input){
// the new url modified with the input
var url = "http://api.prg2021.texttechnologylab.org/pos?party=" + input
$.ajax({
        type: "GET",
        url: url,
        // does something on succeeding API access
        success: function (data) {
            // if nothing ( undefined ) is returned the user will get an alert to change the input
            if (data.result == undefined) {
                alert("No result found \nprobably wrong input");
            }
            // adds the data 1 by 1 into the chart till its done and ready to visualize
            var pos = data.result;
            var label;
            var count;
            for (let i = 0; i < pos.length; i++) {
                label = pos[i].pos;
                count = pos[i].count;
                addData(PieChartPOS, label, count);
            }
        },
        // prints the error on the console
        error: function (error) {
            console.log("Error ${error}");
        }
        });
}

/*
* the parameter of the function is the input from the textfield 'fraction'
* filters the API Request with the input
*/
function filterPOSFraction(input){
// the new url modified with the input
var url = "http://api.prg2021.texttechnologylab.org/pos?fraction=" + input
$.ajax({
        type: "GET",
        url: url,
        // does something on succeeding API access
        success: function (data) {
            // if nothing ( undefined ) is returned the user will get an alert to change the input
            if (data.result == undefined) {
                alert("No result found \nprobably wrong input");
            }
            // adds the data 1 by 1 into the chart till its done and ready to visualize
            var pos = data.result;
            var label;
            var count;
            for (let i = 0; i < pos.length; i++) {
                label = pos[i].pos;
                count = pos[i].count;
                addData(PieChartPOS, label, count);
            }
        },
        // prints the error on the console
        error: function (error) {
            console.log("Error ${error}");
        }
        });
}

/*
* the parameter of the function is the input from the textfield 'speaker ID'
* filters the API Request with the input
*/
function filterPOSID(input){
// the new url modified with the input
var url = "http://api.prg2021.texttechnologylab.org/pos?user=" + input
$.ajax({
        type: "GET",
        url: url,
        // does something on succeeding API access
        success: function (data) {
             // if nothing ( undefined ) is returned the user will get an alert to change the input
            if (data.result == undefined) {
                alert("No result found \nprobably wrong input");
            }
            // adds the data 1 by 1 into the chart till its done and ready to visualize
            var pos = data.result;
            var label;
            var count;
            for (let i = 0; i < pos.length; i++) {
                label = pos[i].pos;
                count = pos[i].count;
                addData(PieChartPOS, label, count);
            }
        },
        // prints the error on the console
        error: function (error) {
            console.log("Error ${error}");
        }
        });
}

/*
* the parameter of the function is the input from the textfield 'min count'
* filters the API Request with the input
*/
function filterPOSCount(input){
// the input is a string but it has to be an integer for the url
input = parseInt(input);
// the new url modified with the input
var url = "http://api.prg2021.texttechnologylab.org/pos?minimum=" + input;
$.ajax({
        type: "GET",
        url: url,
        // does something on succeeding API access
        success: function (data) {
            // if nothing ( undefined ) is returned the user will get an alert to change the input
            if (data.result == undefined) {
                alert("No result found \nprobably wrong input");
            }
            // adds the data 1 by 1 into the chart till its done and ready to visualize
            var pos = data.result;
            var label;
            var count;
            for (let i = 0; i < pos.length; i++) {
                label = pos[i].pos;
                count = pos[i].count;
                addData(PieChartPOS, label, count);
            }
        },
        // prints the error on the console
        error: function (error) {
            console.log("Error ${error}");
        }
        });
}