/*
* ajax calls that insert information into the tab content 'PieChartPersons'
* visualisation is a pie-chart
*/

/*
* starts when index.html is opened
* shows all /namedEntities persons with a default minimum count of 50
*/
function ajaxPersons(){
$.ajax({
        type: "GET",
        url: "http://api.prg2021.texttechnologylab.org/namedEntities?minimum=50",
        // does something on succeeding API access
        success: function (data) {
            // adds the data 1 by 1 into the chart till its done and ready to visualize
            var persons = data.result[0].persons;
            var count;
            var element;
            for (let i = 0; i < persons.length; i++) {
                count = persons[i].count;
                element = persons[i].element;
                addData(PieChartPersons, element , count);
            }
        },
        // prints the error on the console
        error: function (error) {
            console.log("Error ${error}");
        }
        });
}

/*
* the parameter of the function is the input from the textfield 'party'
* filters the API Request with the input
* default minimum count is 10 because of the long loading time
*/
function filterPersonsParty(input){
// the new url modified with the input
var url = "http://api.prg2021.texttechnologylab.org/namedEntities?minimum=10&party=" + input;
$.ajax({
        type: "GET",
        url: url,
        // does something on succeeding API access
        success: function (data) {
            // if nothing ( undefined ) is returned the user will get an alert to change the input
            if (data.result == undefined) {
                alert("No result found \nprobably wrong input");
            }
            // adds the data 1 by 1 into the chart till its done and ready to visualize
            var persons = data.result[0].persons;
            var count;
            var element;
            for (let i = 0; i < persons.length; i++) {
                count = persons[i].count;
                element = persons[i].element;
                addData(PieChartPersons, element , count);
            }
        },
        // prints the error on the console
        error: function (error) {
            console.log("Error ${error}");
        }
        });

}

/*
* the parameter of the function is the input from the textfield 'fraction'
* filters the API Request with the input
* default minimum count is 10 because of the long loading time
*/
function filterPersonsFraction(input){
// the new url modified with the input
var url = "http://api.prg2021.texttechnologylab.org/namedEntities?minimum=10&fraction=" + input;
$.ajax({
        type: "GET",
        url: url,
        // does something on succeeding API access
        success: function (data) {
            // if nothing ( undefined ) is returned the user will get an alert to change the input
            if (data.result == undefined) {
                alert("No result found \nprobably wrong input");
            }
            // adds the data 1 by 1 into the chart till its done and ready to visualize
            var persons = data.result[0].persons;
            var count;
            var element;
            for (let i = 0; i < persons.length; i++) {
                count = persons[i].count;
                element = persons[i].element;
                addData(PieChartPersons, element , count);
            }
        },
        // prints the error on the console
        error: function (error) {
            console.log("Error ${error}");
        }
        });
}

/*
* the parameter of the function is the input from the textfield 'speaker ID'
* filters the API Request with the input
*/
function filterPersonsID(input){
// the new url modified with the input
var url = "http://api.prg2021.texttechnologylab.org/namedEntities?user=" + input;
$.ajax({
        type: "GET",
        url: url,
        // does something on succeeding API access
        success: function (data) {
            // if nothing ( undefined ) is returned the user will get an alert to change the input
            if (data.result == undefined) {
                alert("No result found \nprobably wrong input");
            }
            // adds the data 1 by 1 into the chart till its done and ready to visualize
            var persons = data.result[0].persons;
            var count;
            var element;
            for (let i = 0; i < persons.length; i++) {
                count = persons[i].count;
                element = persons[i].element;
                addData(PieChartPersons, element , count);
            }
        },
        // prints the error on the console
        error: function (error) {
            console.log("Error ${error}");
        }
        });
}
/*
* the parameter of the function is the input from the textfield 'min count'
* filters the API Request with the input
*/
function filterPersonsCount(input){
// the input is a string but it has to be an integer for the url
input = parseInt(input);
// the new url modified with the input
var url = "http://api.prg2021.texttechnologylab.org/namedEntities?minimum=" + input;
$.ajax({
        type: "GET",
        url: url,
        // does something on succeeding API access
        success: function (data) {
            // if nothing ( undefined ) is returned the user will get an alert to change the input
            if (data.result == undefined) {
                alert("No result found \nprobably wrong input");
            }
            // adds the data 1 by 1 into the chart till its done and ready to visualize
            var persons = data.result[0].persons;
            var count;
            var element;
            for (let i = 0; i < persons.length; i++) {
                count = persons[i].count;
                element = persons[i].element;
                addData(PieChartPersons, element , count);
            }
        },
        // prints the error on the console
        error: function (error) {
            console.log("Error ${error}");
        }
        });

}
