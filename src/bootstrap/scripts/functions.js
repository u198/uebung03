//functions used in other Javascripts

/*
* adds label and data into the given chart.
* also generates a random color for every push
*/
function addData(chart, label, data) {
    var randomColor;
    // pushes the given label into the chart labels
    chart.data.labels.push(label);
    // pushes the given data into the chart data
    chart.data.datasets.forEach((dataset) => {
        dataset.data.push(data);
        // generates a random hex-colorcode for better visualisation
        randomColor = Math.floor(Math.random()*16777215).toString(16);
        dataset.backgroundColor.push("#" + randomColor);
    });
    chart.update();
}

// does the same as addData(chart, label, data) but without the Color
function addDataWithoutColor(chart, label, data) {
    chart.data.labels.push(label);
    chart.data.datasets.forEach((dataset) => {
        dataset.data.push(data);
    });
    chart.update();
}

// removes all labels and data from a chart till the chart is empty
function removeData(chart) {
    while(chart.data.datasets[0].data.length > 0) {
    chart.data.labels.pop();
    chart.data.datasets.forEach((dataset) => {
        dataset.data.pop();
    });
    }
    chart.update();
}

// opens the tabcontent of a tablink
function openTab(evt, name){
var i;
var tabContent;
var tabLinks;

tabContent = document.getElementsByClassName("tabcontent");
for ( i = 0; i < tabContent.length; i++) {
    tabContent[i].style.display = "none";
}
tabLinks = document.getElementsByClassName("tablinks");
for ( i = 0; i < tabLinks.length; i++) {
    tabLinks[i].className = tabLinks[i].className.replace(" active", "");
}
document.getElementById(name).style.display = "block";
evt.currentTarget.className += " active";
}

/*
* returns the average length of a given array
* used in ajaxAverage()
*/
function averageLength(arr){
    var total = 0;
    var i = 0;

    for (i = 0; i < arr.length; i++) {
    // adds the character length of the given array to total
        total += arr[i].length;
    }
    // divides the total with the amount of arrays to get the average
    return total / i;
}