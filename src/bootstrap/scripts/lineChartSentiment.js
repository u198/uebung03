// Line-Chart configuration and data for Sentiment
var ctx = document.getElementById("LineSen");
var LineSen = new Chart(ctx, {
    type: "line",
    data: {
        labels: [],
        datasets: [{
            data: [],
            backgroundColor: 'rgb(255, 99, 132)'
        }],
    },
    options: {
        scales: {
            yAxes: [{
            ticks: {min: 0, max:400},
            scaleLabel: {
            display: true,
            labelString: "Frequency"
            }
            }],
            xAxes: [{
            scaleLabel: {
            display: true,
            labelString: "Sentiment"
            }
            }]
        },
        legend: {
            display: false
        }
    }
});
