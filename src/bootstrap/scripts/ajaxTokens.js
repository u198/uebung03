/*
* ajax calls that insert information into the tab content 'PieChartTokens'
* visualisation is a pie-chart
*/

/*
* when the 'Tokens' tab is clicked on, the function starts to add the data from the API /tokens
* default minimum count is 2000 because of the long loading time
*/
function ajaxTokens2000(){
$.ajax({
        type: "GET",
        url: "http://api.prg2021.texttechnologylab.org/tokens?minimum=2000",
        // does something on succeeding API access
        success: function (data) {
            // adds the data 1 by 1 into the chart till its done and ready to visualize
            var tokens = data.result;
            var token;
            var count;
            var i;
            for (i = 0; i < tokens.length; i++) {
                token = tokens[i].token;
                count = tokens[i].count;
                addData(PieChartTokens, token, count);
            }
        },
        // prints the error on the console
        error: function (error) {
            console.log("Error ${error}");
        }
        });
}

/*
* the parameter of the function is the input from the textfield 'party'
* filters the API Request with the input
* default minimum count is 1000 because of the long loading time
*/
function filterTokensParty(input){
// the new url modified with the input
var url = "http://api.prg2021.texttechnologylab.org/tokens?minimum=1000&party=" + input;
$.ajax({
        type: "GET",
        url: url,
        // does something on succeeding API access
        success: function (data) {
            // if nothing ( undefined ) is returned the user will get an alert to change the input
            if (data.result == undefined) {
                alert("No result found \nprobably wrong input");
            }
            // adds the data 1 by 1 into the chart till its done and ready to visualize
            var tokens = data.result;
            var token;
            var count;
            for (let = i = 0; i < tokens.length; i++) {
                token = tokens[i].token;
                count = tokens[i].count;
                addData(PieChartTokens, token, count);
            }
        },
        // prints the error on the console
        error: function (error) {
            console.log("Error ${error}");
        }
        });
}

/*
* the parameter of the function is the input from the textfield 'fraction'
* filters the API Request with the input
* default minimum count is 1000 because of the long loading time
*/
function filterTokensFraction(input){
// the new url modified with the input
var url = "http://api.prg2021.texttechnologylab.org/tokens?minimum=1000&fraction=" + input;
$.ajax({
        type: "GET",
        url: url,
        // does something on succeeding API access
        success: function (data) {
            // if nothing ( undefined ) is returned the user will get an alert to change the input
            if (data.result == undefined) {
                alert("No result found \nprobably wrong input");
            }
            // adds the data 1 by 1 into the chart till its done and ready to visualize
            var tokens = data.result;
            var token;
            var count;
            for (let = i = 0; i < tokens.length; i++) {
                token = tokens[i].token;
                count = tokens[i].count;
                addData(PieChartTokens, token, count);
            }
        },
        // prints the error on the console
        error: function (error) {
            console.log("Error ${error}");
        }
        });
}

/*
* the parameter of the function is the input from the textfield 'speaker ID'
* filters the API Request with the input
* default minimum count is 100 because of the long loading time
*/
function filterTokensID(input){
// the new url modified with the input
var url = "http://api.prg2021.texttechnologylab.org/tokens?minimum=100&user=" + input;
$.ajax({
        type: "GET",
        url: url,
        // does something on succeeding API access
        success: function (data) {
            // if nothing ( undefined ) is returned the user will get an alert to change the input
            if (data.result == undefined) {
                alert("No result found \nprobably wrong input");
            }
            // adds the data 1 by 1 into the chart till its done and ready to visualize
            var tokens = data.result;
            var token;
            var count;
            for (let = i = 0; i < tokens.length; i++) {
                token = tokens[i].token;
                count = tokens[i].count;
                addData(PieChartTokens, token, count);
            }
        },
        // prints the error on the console
        error: function (error) {
            console.log("Error ${error}");
        }
        });
}

/*
* the parameter of the function is the input from the textfield 'min count'
* filters the API Request with the input
*/
function filterTokensCount(input){
// the input is a string but it has to be an integer for the url
input = parseInt(input);
// the new url modified with the input
var url = "http://api.prg2021.texttechnologylab.org/tokens?minimum=" + input;
$.ajax({
        type: "GET",
        url: url,
        // does something on succeeding API access
        success: function (data) {
            // if nothing ( undefined ) is returned the user will get an alert to change the input
            if (data.result == undefined) {
                alert("No result found \nprobably wrong input");
            }
            // adds the data 1 by 1 into the chart till its done and ready to visualize
            var tokens = data.result;
            var token;
            var count;
            for (let = i = 0; i < tokens.length; i++) {
                token = tokens[i].token;
                count = tokens[i].count;
                addData(PieChartTokens, token, count);
            }
        },
        // prints the error on the console
        error: function (error) {
            console.log("Error ${error}");
        }
        });
}