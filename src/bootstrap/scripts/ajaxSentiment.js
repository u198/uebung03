/*
* ajax calls that insert information into the tab content 'LineSen'
* visualisation is a line-chart
*/

// when the 'Sentiment' tab is clicked on, the function starts to add the data from the API /sentiment
function ajaxSentiment(){
$.ajax({
        type: "GET",
        url: "http://api.prg2021.texttechnologylab.org/sentiment",
        // does something on succeeding API access
        success: function (data) {
            var sen = data.result;
            var count;
            var sentiment;
            // sorts the result array from the highest number to the lowest
            sen.sort(function (a, b) {
                return parseFloat(b.sentiment) - parseFloat(a.sentiment);
            });
            // adds the data 1 by 1 into the chart till its done and ready to visualize
            for (let i = 0; i < sen.length; i++) {
                count = sen[i].count;
                sentiment = parseFloat(sen[i].sentiment);
                addDataWithoutColor(LineSen, sentiment, count);
            }
        },
        // prints the error on the console
        error: function (error) {
            console.log("Error ${error}");
        }
        });
}

/*
* the parameter of the function is the input from the textfield 'party'
* filters the API Request with the input
*/
function filterSenParty(input){
// the new url modified with the input
var url = "http://api.prg2021.texttechnologylab.org/sentiment?party=" + input;
$.ajax({
        type: "GET",
        url: url,
        // does something on succeeding API access
        success: function (data) {
            // if nothing ( undefined ) is returned the user will get an alert to change the input
            if (data.result == undefined) {
                alert("No result found \nprobably wrong input");
            }
            var sen = data.result;
            var count;
            var sentiment;
            // sorts the result array from the highest number to the lowest
            sen.sort(function (a, b) {
                return parseFloat(b.sentiment) - parseFloat(a.sentiment);
            });
            // adds the data 1 by 1 into the chart till its done and ready to visualize
            for (let i = 0; i < sen.length; i++) {
                count = sen[i].count;
                sentiment = sen[i].sentiment;
                addDataWithoutColor(LineSen, sentiment, count);
            }
        },
        error: function (error) {
            console.log("Error ${error}");
        }
        });

}

/*
* the parameter of the function is the input from the textfield 'fraction'
* filters the API Request with the input
*/
function filterSenFraction(input){
// the new url modified with the input
var url = "http://api.prg2021.texttechnologylab.org/sentiment?fraction=" + input;
$.ajax({
        type: "GET",
        url: url,
        // does something on succeeding API access
        success: function (data) {
            // if nothing ( undefined ) is returned the user will get an alert to change the input
            if (data.result == undefined) {
                alert("No result found \nprobably wrong input");
            }
            var sen = data.result;
            var count;
            var sentiment;
            // sorts the result array from the highest number to the lowest
            sen.sort(function (a, b) {
                return parseFloat(b.sentiment) - parseFloat(a.sentiment);
            });
            // adds the data 1 by 1 into the chart till its done and ready to visualize
            for (let i = 0; i < sen.length; i++) {
                count = sen[i].count;
                sentiment = sen[i].sentiment;
                addDataWithoutColor(LineSen, sentiment, count);
            }
        },
        // prints the error on the console
        error: function (error) {
            console.log("Error ${error}");
        }
        });

}

/*
* the parameter of the function is the input from the textfield 'speaker ID'
* filters the API Request with the input
*/
function filterSenID(input){
// the new url modified with the input
var url = "http://api.prg2021.texttechnologylab.org/sentiment?user=" + input;
$.ajax({
        type: "GET",
        url: url,
        // does something on succeeding API access
        success: function (data) {
            // if nothing ( undefined ) is returned the user will get an alert to change the input
            if (data.result == undefined) {
                alert("No result found \nprobably wrong input");
            }
            var sen = data.result;
            var count;
            var sentiment;
            // sorts the result array from the highest number to the lowest
            sen.sort(function (a, b) {
                return parseFloat(b.sentiment) - parseFloat(a.sentiment);
            });
            // adds the data 1 by 1 into the chart till its done and ready to visualize
            for (let i = 0; i < sen.length; i++) {
                count = sen[i].count;
                sentiment = sen[i].sentiment;
                addDataWithoutColor(LineSen, sentiment, count);
            }
        },
        // prints the error on the console
        error: function (error) {
            console.log("Error ${error}");
        }
        });

}